use test_schema;

DROP TABLE IF EXISTS `t_time`;
CREATE TABLE `t_time` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`c_time` TIMESTAMP(6) NULL DEFAULT NULL,
	`c_timestamp` TIMESTAMP NULL DEFAULT NULL,
	`c_year` YEAR NULL DEFAULT NULL,
	`c_date` DATE NULL DEFAULT NULL,
	`c_time1` TIME NULL DEFAULT NULL,
	`c_datetime` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`id`) USING BTREE
);

INSERT INTO `t_time` VALUES (1, '2022-12-16 11:04:50.004300', '2022-12-16 11:29:52', '2024', '2024-02-17', '18:34:43', '2024-02-17 18:34:46');
INSERT INTO `t_time` VALUES (2, '2022-12-16 11:04:50.004300', '2022-12-16 11:04:50', '2024', '2024-02-17', '18:34:43', '2024-02-17 18:34:46');
INSERT INTO `t_time` VALUES (3, '2022-12-16 11:14:50.004300', '2022-12-16 11:39:52', '2024', '2024-02-17', '18:34:44', '2024-02-17 18:34:47');
INSERT INTO `t_time` VALUES (4, '2022-12-16 11:14:50.004300', '2022-12-16 11:39:52', '2024', '2024-02-17', '18:34:45', '2024-02-17 18:34:48');
INSERT INTO `t_time` VALUES (5, '2024-02-20 16:09:03.000000', '2024-02-20 16:09:05', '2024', '2024-02-20', '16:09:09', '2024-02-20 16:09:10');
